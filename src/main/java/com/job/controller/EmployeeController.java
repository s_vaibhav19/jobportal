package com.job.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.job.pojo.AppliedJobInfo;
import com.job.pojo.CmpJobPostMaster;
import com.job.pojo.JobUserInfoMaster;
import com.job.service.EmployeeServiceInterface;

@Controller
public class EmployeeController {
	
	@Autowired
	HttpSession session;
	
	@Autowired
	EmployeeServiceInterface employeeService;

	@RequestMapping(value = "userPage.htm")
	public String userHome() {
		if (null != session.getAttribute("userType") && session.getAttribute("userType").equals("NU2")) {
			return "employee/userPage";
		}else{
			return "redirect:welcomePage.htm";
		}
	}

	@RequestMapping(value = "myProfile.htm")
	public ModelAndView userProfile() {
		if(null != session.getAttribute("portalId")){
			JobUserInfoMaster userMaster = employeeService.getUserDetails(session.getAttribute("portalId"));
			ModelAndView userView = new ModelAndView("employee/userProfile");
			
			userView.addObject("userTable", userMaster);
			return userView;
		}
		return new ModelAndView("welcomePage");
	}

	@RequestMapping(value = "jobSearch.htm")
	public ModelAndView userJobSearch() {
		
		ModelAndView jobView = new ModelAndView("employee/searchPage");
		int portalID = (Integer) session.getAttribute("portalId");
		List<CmpJobPostMaster> viewAllJobs = employeeService.getPostedJobs(portalID);
		jobView.addObject("jobList", viewAllJobs);

		return jobView;
	}

	@RequestMapping(value = "appliedJobs.htm")
	public ModelAndView userActivities() {

		int portalId = (Integer) session.getAttribute("portalId");
		ModelAndView appliedJobsView = new ModelAndView("employee/appliedJobs");
		List<AppliedJobInfo> appliedInfo = employeeService.getAppliedJobs(portalId); 
		appliedJobsView.addObject("appliedList", appliedInfo);
		
		return appliedJobsView;
	}
	
	@RequestMapping(value="applyThisJob.htm")
	public @ResponseBody String applyJob(HttpServletRequest request,HttpServletResponse response){
		int jobId = Integer.parseInt(request.getParameter("jobID").toString().trim());
		int portalId = (Integer) session.getAttribute("portalId");
		return employeeService.applyJob(jobId,portalId);
	}
	
	@RequestMapping(value = "searhJobCreteria.htm")
	public ModelAndView searhJobCreteria(HttpServletRequest request, HttpServletResponse response){
		
		String location = request.getParameter("locations").trim().toLowerCase();
		String jobType = request.getParameter("jobType").trim();
		String skillSet = request.getParameter("skillSet").trim();
		int portalID = (Integer) session.getAttribute("portalId");
		
		ModelAndView jobView = new ModelAndView("employee/searchPage");
		
		List<CmpJobPostMaster> viewAllJobs = employeeService.getJobSearch(location, jobType, skillSet);
		jobView.addObject("jobList", viewAllJobs);
		
		return jobView;
	}
	
	@RequestMapping(value="updateUserProfile.htm")
	public @ResponseBody String updateUserProfile(HttpServletRequest request,HttpServletResponse response){
		
		String userFirstName = request.getParameter("userFirstName");
		String userLastName = request.getParameter("userLastName");
		String userEmail = request.getParameter("userEmail");
		String userNo = request.getParameter("userNo");
		int userExp = Integer.parseInt(request.getParameter("userExp"));
		int userSalary = Integer.parseInt(request.getParameter("userSalary"));
		int portalID = (Integer) session.getAttribute("portalId");
		String location = request.getParameter("userLocation");
		String coverLetter = request.getParameter("coverLetter");
		
		JobUserInfoMaster masterInfo = new JobUserInfoMaster();
		masterInfo.setContactNo(userNo);
		masterInfo.setEmail(userEmail);
		masterInfo.setExperience(userExp);
		masterInfo.setFirstName(userFirstName);
		masterInfo.setLastName(userLastName);
		masterInfo.setPortalID(portalID);
		masterInfo.setSalary(userSalary);
		masterInfo.setCoverLetter(coverLetter);
		masterInfo.setLocation(location);
		
		return employeeService.updateUserProfile(masterInfo);
	}
}
