package com.job.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.job.pojo.JobEmpCmpyInfoMaster;
import com.job.pojo.JobUserInfoMaster;
import com.job.service.AdminServiceInterface;

@Controller
public class AdminController {

	@Autowired
	HttpSession session;

	@Autowired
	AdminServiceInterface adminService;

	@RequestMapping(value = "adminLogin.htm")
	public String empHomePage() {

		if (null != session.getAttribute("userType") && session.getAttribute("userType").equals("AD0")) {
			return "admin/adminWelcomePage";
		} else {
			return "redirect:welcomePage.htm";
		}
	}

	@RequestMapping(value = "adminCmpyList.htm")
	public ModelAndView adminCmpyList() {

		ModelAndView viewCompanies = new ModelAndView("admin/companyList");
		List<JobEmpCmpyInfoMaster> companyiesList = adminService.getAllCmpyList();
		viewCompanies.addObject("cmpyList", companyiesList);
		return viewCompanies;
	}

	@RequestMapping(value = "adminUserList.htm")
	public ModelAndView adminUserList() {
		ModelAndView userlist = new ModelAndView("admin/userList");
		List<JobUserInfoMaster> userInfoList = adminService.getAllUserList();
		userlist.addObject("userList", userInfoList);
		return userlist;
	}

	@RequestMapping(value = "deactiveCmpList.htm")
	public ModelAndView getDeactiveCmpList() {

		ModelAndView viewCompanies = new ModelAndView("admin/companyDeactiveList");
		List<JobEmpCmpyInfoMaster> companyiesList = adminService.getAllDeactiveCmpyList();
		viewCompanies.addObject("cmpyList", companyiesList);
		return viewCompanies;
	}

	@RequestMapping(value = "deactiveUserList")
	public ModelAndView getDeactiveUserList() {
		ModelAndView userlist = new ModelAndView("admin/userDeactiveList");
		List<JobUserInfoMaster> userInfoList = adminService.getAllDeactiveUserList();
		userlist.addObject("userList", userInfoList);
		return userlist;
	}
	
	@RequestMapping(value="deactivateUser.htm")
	public @ResponseBody String deactivateUSer(HttpServletRequest request,HttpServletResponse response){
		int portalID = Integer.parseInt(request.getParameter("portalID"));
		int activeFlag = Integer.parseInt(request.getParameter("activityFlag"));
		return adminService.deactivateUSer(portalID,activeFlag);
	}
}
