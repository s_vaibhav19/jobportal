package com.job.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.job.pojo.CmpJobPostMaster;
import com.job.pojo.JobEmpCmpyInfoMaster;
import com.job.service.EmployerServiceInterface;

@Controller
public class EmployerController {
	
	@Autowired
	HttpSession session;

	@Autowired
	EmployerServiceInterface employerService;
	
	@RequestMapping(value = "employerLogin.htm")
	public String empHomePage(){
		if (null != session.getAttribute("userType") && session.getAttribute("userType").equals("EP1")) {
			return "employer/employerPage";
		}else{
			return "redirect:welcomePage.htm";
		}
	}
	
	@RequestMapping(value = "cmpyProfile.htm")
	public ModelAndView empProfile(){
		if(null != session.getAttribute("portalId")){
			ModelAndView empView = new ModelAndView("employer/profile");
			JobEmpCmpyInfoMaster cmpInfo =	employerService.getCmpyDetails(session.getAttribute("portalId"));
			empView.addObject("cmpyInfo", cmpInfo);
		return empView;
		}else{
			return new ModelAndView("welcomePage");
		}
	}
	
	@RequestMapping(value = "jobPost.htm")
	public String empPostJob(HttpServletResponse response, HttpServletRequest request){
			return "employer/postJob";
	}
	
	@RequestMapping(value = "jobRequest.htm")
	public ModelAndView empJobRequest(HttpServletRequest request,HttpServletResponse response){
		
		ModelAndView jobRequestView = new ModelAndView("employer/jobRequest");
		int portalID = (Integer) session.getAttribute("portalId");
		List<CmpJobPostMaster> jobInfo = employerService.getJobDetails(portalID);
		jobRequestView.addObject("jobInfo", jobInfo);
		
		
		return jobRequestView;
	}
	
	@RequestMapping(value = "jobPostAction.htm")
	public @ResponseBody String jobPosAction(HttpServletRequest request, HttpServletResponse response){

		String jobTitle = request.getParameter("jobTitle").trim();
		String skillSet =request.getParameter("skillSet").trim();
		int salary = Integer.parseInt(request.getParameter("salary").trim());
		String jobType = request.getParameter("jobType").trim();
		int portalID = (Integer) session.getAttribute("portalId");
		int vacancy = Integer.parseInt(request.getParameter("vacancy"));
		String location = request.getParameter("location");
		String descriptoin = request.getParameter("jobDescriptions");
		
		CmpJobPostMaster jobPostMaster = new CmpJobPostMaster();
		jobPostMaster.setActiveFlag(1);
		jobPostMaster.setJobDescription(descriptoin);
		jobPostMaster.setJobTitle(jobTitle);
		jobPostMaster.setJobType(jobType);
		jobPostMaster.setLocation(location);
		jobPostMaster.setPortalID(portalID);
		jobPostMaster.setSalary(salary);
		jobPostMaster.setSkillSet(skillSet);
		jobPostMaster.setVacancy(vacancy);
		jobPostMaster.setTimeStamp("");
		
		employerService.addJob(jobPostMaster);
	
		return "";
	}
	
	@RequestMapping(value="getUserJobList.htm")
	public @ResponseBody String retriveJobList(HttpServletRequest request,HttpServletResponse response){
		
		int jobId= Integer.parseInt(request.getParameter("jobId").toString());
		int portalID = (Integer) session.getAttribute("portalId");
		
		return employerService.getAppliedJobList(portalID,jobId);
	}
	@RequestMapping(value="updateUSerFeedBack.htm")
	public void updateFeedBack(HttpServletRequest request,HttpServletResponse response){
		String feedBackMsg = request.getParameter("feedMsg");
		String status = request.getParameter("staus");
		int portalId = Integer.parseInt(request.getParameter("portalID"));
		int jobId = Integer.parseInt(request.getParameter("divID"));
		
		employerService.updateFeedBack(jobId,portalId,status,feedBackMsg);
	}
	
	@RequestMapping(value="updateCmpyProfile.htm")
	public @ResponseBody String updateCmpyProfile(HttpServletRequest request,HttpServletResponse response){
		
		int portalId = (Integer) session.getAttribute("portalId");
		String regNo = request.getParameter("empRegNo");
		String contactNo = request.getParameter("empContactNo");
		String cmpyName = request.getParameter("empCmpyName");
		String email = request.getParameter("empEmail");
		String website = request.getParameter("empWebSite");
		String description = request.getParameter("empDescription");
		String password = request.getParameter("empPassKey");
		
		
		JobEmpCmpyInfoMaster masterInfo = new JobEmpCmpyInfoMaster();
		masterInfo.setCompanyName(cmpyName);
		masterInfo.setContactNo(contactNo);
		masterInfo.setDescription(description);
		masterInfo.setEmail(email);
		masterInfo.setPortalID(portalId);
		masterInfo.setRegNo(regNo);
		masterInfo.setWebSiteLink(website);
		
		return employerService.updateCmpyProfile(masterInfo);
	}
}
