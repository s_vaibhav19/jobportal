package com.job.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.job.pojo.JobEmpCmpyInfoMaster;
import com.job.pojo.JobUserInfoMaster;
import com.job.pojo.LoginTable;
import com.job.service.HomeServiceInterface;

@Controller
public class HomeController {


	@Autowired
	HttpSession session;
	
	@Autowired
	HomeServiceInterface homeService;

	@RequestMapping(value = "welcomePage.htm")
	public String home(Locale locale, Model model) {

		return "welcomePage";
	}

	@RequestMapping(value = "loginAction.htm")
	public @ResponseBody String checkLogin(HttpServletResponse response, HttpServletRequest request) {
		
		String userName = request.getParameter("userKey").trim();
		String password = request.getParameter("passkey").trim();
		
		LoginTable loginTable = homeService.checkLogin(userName,password);
		String firstName = homeService.getFirstName(loginTable.getPortalID(),loginTable.getUserType());
		
		session.setAttribute("firstName", firstName);
		session.setAttribute("portalId", loginTable.getPortalID());
		session.setAttribute("userName", userName);
		session.setAttribute("userType", loginTable.getUserType());
		
		return "divertSession";
	}

	@RequestMapping(value = "forwardRequest.htm")
	public String divertingPageURL() {
		if(null != session.getAttribute("userType")){
		if (session.getAttribute("userType").equals("AD0")) {
			return "redirect:adminLogin.htm";
		} else if (session.getAttribute("userType").equals("EP1")) {
			return "redirect:employerLogin.htm";
		} else if (session.getAttribute("userType").equals("NU2")) {
			return "redirect:userPage.htm";
		} else {
			return "redirect:welcomePage.htm";
		}
		}else{
			return "redirect:welcomePage.htm";
		}
	}

	@RequestMapping(value = "logout.htm")
	public String destroySession(HttpServletRequest request, HttpServletResponse response) {

		session.invalidate();

		return "redirect:welcomePage.htm";
	}
	
	@RequestMapping(value = "registerUser.htm")
	public @ResponseBody String registerUser(HttpServletRequest request,HttpServletResponse response){
		
		String userName = request.getParameter("userEmail").trim();
		String password = request.getParameter("userPassword").trim();
		String firstName = request.getParameter("firstName").trim();
		String lastName = request.getParameter("lastName").trim();
		String mobileNo = request.getParameter("userNo").trim();
		
		if(homeService.validateRequest(userName)){
			LoginTable loginTable = new LoginTable();
			loginTable.setActiveFlag(1);
			loginTable.setUserType("NU2");
			loginTable.setUserId(userName);
			loginTable.setPassword(password);
			
			int portalID = homeService.createLogin(loginTable);
			
			JobUserInfoMaster infoMaster = new JobUserInfoMaster();
			infoMaster.setContactNo(mobileNo);
			infoMaster.setCoverLetter(" ");
			infoMaster.setEmail(userName);
			infoMaster.setExperience(0);
			infoMaster.setFirstName(firstName);
			infoMaster.setLastName(lastName);
			infoMaster.setLocation(" ");
			infoMaster.setPortalID(portalID);
			infoMaster.setSalary(0);
			
			homeService.updateUserProfile(infoMaster);
			session.setAttribute("userType", "NU2");
			session.setAttribute("firstName", infoMaster.getFirstName());
			session.setAttribute("portalId", portalID);
			session.setAttribute("userName", userName);
		}
		
		return "divertSession";
	}
	
	@RequestMapping(value = "registerCmpy.htm")
	public @ResponseBody String registerCmpy(HttpServletRequest request,HttpServletResponse response){
		
		String cmpyEmail = request.getParameter("cmpyEmail").trim();
		String password = request.getParameter("cmpyPasskey").trim();
		String cmpyName = request.getParameter("cmpyName").trim();
		String regNo = request.getParameter("regNo").trim();
		String cmpyNo = request.getParameter("cmpyNo").trim();
		
		if(homeService.validateRequest(cmpyEmail)){
			LoginTable loginTable = new LoginTable();
			loginTable.setActiveFlag(1);
			loginTable.setUserType("EP1");
			loginTable.setUserId(cmpyEmail);
			loginTable.setPassword(password);
			
			int portalID = homeService.createLogin(loginTable);
			
			JobEmpCmpyInfoMaster empMaster = new JobEmpCmpyInfoMaster();
			empMaster.setCompanyName(cmpyName);
			empMaster.setContactNo(cmpyNo);
			empMaster.setDescription(" ");
			empMaster.setEmail(cmpyEmail);
			empMaster.setPortalID(portalID);
			empMaster.setRegNo(regNo);
			empMaster.setWebSiteLink("https://www.google.com");
			
			homeService.updateCompanyProfile(empMaster);
			
			session.setAttribute("userType", "EP1");
			session.setAttribute("firstName", empMaster.getCompanyName());
			session.setAttribute("portalId", portalID);
			session.setAttribute("userName", cmpyEmail);
		}
		
		return "divertSession";
	}
}
