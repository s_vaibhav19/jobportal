package com.job.util;

import org.springframework.stereotype.Component;

import com.job.pojo.JobUserInfoMaster;

@Component("queryProvider")
public class QuerysProvider {

	public String getAppliedJob(int portalID) {

		return "select aj.JobID as jobId,aj.Status as status,aj.FeedBackMsg as message,cm.JobTitle as jobTitle,cm.Location as location from applyJobs aj,cmp_job_post_master cm where aj.PortalID = "
				+ portalID + " and aj.JobID = cm.JobId;";
	}

	public String getUSerDetailsQuery(int portalID, int jobId) {

		return "select PortalID As portalID,FirstName As firstName,LastName As lastName,Email As email,ContactNo As contactNo,Experience As experience"
				+ " from job_userinfo_master where PortalID in(select PortalID from applyjobs where JobID=" + jobId + ")";
	}

	public String getUserUpdateQuery(JobUserInfoMaster masterInfo) {

		return "update job_userinfo_master set ContactNo=\"" + masterInfo.getContactNo() + "\",CoverLetter=\"" + masterInfo.getCoverLetter()
				+ "\",Email=\"" + masterInfo.getEmail() + "\",Experience=" + masterInfo.getExperience() + ",FirstName=\"" + masterInfo.getFirstName()
				+ "\",LastName=\"" + masterInfo.getLastName() + "\",Location=\"" + masterInfo.getLocation() + "\",Salary=" + masterInfo.getSalary()
				+ " where PortalID=" + masterInfo.getPortalID() + ";";
	}

	public String getCmpyList(int flag) {

		return "select PortalID as portalID,CompanyName as companyName,ContactNo as contactNo,Descriptions as description, Email as email,RegNo as regNo,WebSiteLink as webSiteLink from job_emp_cmpyinfo_master where PortalID in(select PortalID from logintable where ActiveFlag="
				+ flag + " and UserType =\"EP1\");";
	}

	public String getUserList(int flag) {
		return "select PortalID as portalID,ContactNo as contactNo,CoverLetter as coverLetter,Email as email, Experience as experience,FirstName as firstName,LastName as lastName,Location as location,Salary as salary from job_userinfo_master where PortalID in(select PortalID from logintable where ActiveFlag="
				+ flag + " and UserType =\"NU2\");";
	}

	public String getActDact(int portalID, int activeFlag) {
		return "update logintable set ActiveFlag="+activeFlag+" where PortalID="+portalID+";";
	}

	public String getJobSearchlist(int portalID) {
		
		return "select JobID as jobId,PortalID as portalID,JobTitle as jobTitle,JobDescription as jobDescription,SkillSet as skillSet,Location as location,salary as salary,NoOfVacancy as vacancy,JobType as jobType,TimeStamp as timeStamp,activeFlag as activeFlag from cmp_job_post_master where JobID Not in(select JobID from ApplyJobs where PortalID ="+portalID+");";
	}

	public String getAppliedJobID(int jobId) {
		return "select PortalID  from applyjobs where JobID = "+jobId+" and Status = 'Hold';";
	}

}
