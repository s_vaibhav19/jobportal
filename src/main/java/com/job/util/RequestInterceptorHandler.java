package com.job.util;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.beans.factory.annotation.Autowired;

public class RequestInterceptorHandler implements HttpSessionListener{
	
	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		System.out.println("+++++++++++++++++++++++++Session Created+++++++++++++++++++++++++");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		System.out.println("+++++++++++++++++++++++++Session Destroyed+++++++++++++++++++++++++++");
	}
	

}
