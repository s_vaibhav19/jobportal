package com.job.service;

import java.util.List;

import com.job.pojo.AppliedJobInfo;
import com.job.pojo.CmpJobPostMaster;
import com.job.pojo.JobUserInfoMaster;

public interface EmployeeServiceInterface {

	JobUserInfoMaster getUserDetails(Object attribute);

	List<CmpJobPostMaster> getPostedJobs(int portalID);

	String applyJob(int jobId, int portalId);

	List<CmpJobPostMaster> getJobSearch(String location, String jobType, String skillSet);

	List<AppliedJobInfo> getAppliedJobs(int portalId);

	String updateUserProfile(JobUserInfoMaster masterInfo);

}
