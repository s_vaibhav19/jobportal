package com.job.service;

import java.util.List;

import com.job.pojo.CmpJobPostMaster;
import com.job.pojo.JobEmpCmpyInfoMaster;

public interface EmployerServiceInterface {

	JobEmpCmpyInfoMaster getCmpyDetails(Object attribute);

	void addJob(CmpJobPostMaster jobPostMaster);

	List<CmpJobPostMaster> getJobDetails(int portalID);

	String getAppliedJobList(int portalID, int jobId);

	void updateFeedBack(int jobId, int portalId, String status, String feedBackMsg);

	String updateCmpyProfile(JobEmpCmpyInfoMaster masterInfo);

}
