package com.job.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.job.dataSource.EmployeeDSInterface;
import com.job.pojo.AppliedJobInfo;
import com.job.pojo.CmpJobPostMaster;
import com.job.pojo.JobUserInfoMaster;
import com.job.service.EmployeeServiceInterface;

@Service("employeeService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
public class EmployeeServiceImpl implements EmployeeServiceInterface {

	@Autowired
	EmployeeDSInterface employeeDS;
	
	@Override
	public JobUserInfoMaster getUserDetails(Object attribute) {
		
		return employeeDS.getUserDetails(attribute);
	}

	@Override
	public List<CmpJobPostMaster> getPostedJobs(int portalID) {
		
		return employeeDS.getPostedJobs(portalID);
	}

	@Override
	public String applyJob(int jobId, int portalId) {
		return employeeDS.applyJob(jobId,portalId);
	}

	@Override
	public List<CmpJobPostMaster> getJobSearch(String location, String jobType, String skillSet) {
		
		return employeeDS.getJobSearch(location,jobType,skillSet);
	}

	@Override
	public List<AppliedJobInfo> getAppliedJobs(int portalId) {
		
		return employeeDS.getAppliedLob(portalId);
	}

	@Override
	public String updateUserProfile(JobUserInfoMaster masterInfo) {
		
		return employeeDS.updateUserJopb(masterInfo);
	}

}
