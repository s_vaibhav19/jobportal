package com.job.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.job.dataSource.EmployerDSInterface;
import com.job.pojo.CmpJobPostMaster;
import com.job.pojo.JobEmpCmpyInfoMaster;
import com.job.pojo.JobUserInfoMaster;
import com.job.service.EmployerServiceInterface;

@Service("employerService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
public class EmployerServiceImp implements EmployerServiceInterface {

	@Autowired
	EmployerDSInterface employerDSImpl;

	@Override
	public JobEmpCmpyInfoMaster getCmpyDetails(Object attribute) {

		return employerDSImpl.getCmpyDetails(attribute);
	}

	@Override
	public void addJob(CmpJobPostMaster jobPostMaster) {
		employerDSImpl.addJob(jobPostMaster);

	}

	@Override
	public List<CmpJobPostMaster> getJobDetails(int portalID) {

		return employerDSImpl.getJobDetails(portalID);
	}

	@Override
	public String getAppliedJobList(int portalID, int jobId) {

		List<JobUserInfoMaster> jobUserList = employerDSImpl.getUserDetails(portalID, jobId);
		List<Integer> doneJobsNo = employerDSImpl.getResponded(jobId);

		StringBuilder tableValues = new StringBuilder();
		tableValues.append("<thead> <th>Employee Name</th><th>Contact Details</th><th>Experience</th><th>FeedBack</th></thead>");
		tableValues.append("<tfoot> <th>Employee Name</th><th>Contact Details</th><th>Experience</th><th>FeedBack</th></tfoot>");
		for (JobUserInfoMaster jobUserInfoMaster : jobUserList) {
			tableValues.append("<tr>");
			tableValues.append("<td>" + jobUserInfoMaster.getFirstName() + " " + jobUserInfoMaster.getLastName() + "</td>");
			tableValues.append("<td>" + jobUserInfoMaster.getEmail() + "," + jobUserInfoMaster.getContactNo() + "</td>");
			tableValues.append("<td>" + jobUserInfoMaster.getExperience() + "</td>");
			if (doneJobsNo.contains(jobUserInfoMaster.getPortalID())) {
				tableValues.append(
						"<td><button id=\"create-user\" onclick=\"callDialog(" + jobUserInfoMaster.getPortalID() + ")\">Respond</button> </td>");
			} else {
				tableValues.append("<td>Done </td>");
			}
			tableValues.append("</tr>");
		}
		return tableValues.toString();
	}

	@Override
	public void updateFeedBack(int jobId, int portalId, String status, String feedBackMsg) {
		employerDSImpl.updateFeedBAck(jobId, portalId, status, feedBackMsg);

	}

	@Override
	public String updateCmpyProfile(JobEmpCmpyInfoMaster masterInfo) {

		return employerDSImpl.updateCmpyProfile(masterInfo);
	}

}
