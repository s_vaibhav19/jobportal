package com.job.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

import com.job.dataSource.AdminDSInterface;
import com.job.pojo.JobEmpCmpyInfoMaster;
import com.job.pojo.JobUserInfoMaster;
import com.job.service.AdminServiceInterface;

@Service("adminService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
public class AdminServiceImp implements AdminServiceInterface {

	@Autowired
	AdminDSInterface adminDS;
	
	@Override
	public List<JobEmpCmpyInfoMaster> getAllCmpyList() {
		
		return adminDS.getCmpyList();
	}

	@Override
	public List<JobUserInfoMaster> getAllUserList() {
		
		return adminDS.getUserList();
	}

	@Override
	public List<JobEmpCmpyInfoMaster> getAllDeactiveCmpyList() {
		
		return adminDS.getCmpyDeactiveList();
	}

	@Override
	public List<JobUserInfoMaster> getAllDeactiveUserList() {
		
		return adminDS.getDeactiveUserList();
	}

	@Override
	public String deactivateUSer(int portalID,int activeFlag) {
		
		return adminDS.deactivateUSer(portalID,activeFlag);
	}

}
