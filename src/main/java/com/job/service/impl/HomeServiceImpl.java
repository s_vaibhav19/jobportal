package com.job.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

import com.job.dataSource.HomeDSInterface;
import com.job.pojo.JobEmpCmpyInfoMaster;
import com.job.pojo.JobUserInfoMaster;
import com.job.pojo.LoginTable;
import com.job.service.HomeServiceInterface;

@Service("homeService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
public class HomeServiceImpl implements HomeServiceInterface {

	@Autowired
	HomeDSInterface homeDsInterface;

	@Override
	public int createLogin(LoginTable loginTable) {
		
		return homeDsInterface.createLogin(loginTable);
	}

	@Override
	public void updateUserProfile(JobUserInfoMaster infoMaster) {
		homeDsInterface.updateProfile(infoMaster);
	}

	@Override
	public void updateCompanyProfile(JobEmpCmpyInfoMaster empMaster) {
		homeDsInterface.updateCmpyProfile(empMaster);
	}

	@Override
	public LoginTable checkLogin(String userName, String password) {
		
		return homeDsInterface.checkLogin(userName,password);
	}

	@Override
	public boolean validateRequest(String userName) {
		
		return homeDsInterface.validateRequest(userName);
	}

	@Override
	public String getFirstName(int portalID, String userType) {

		return homeDsInterface.getFirstName(portalID,userType);
	}

}
