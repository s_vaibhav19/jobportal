package com.job.service;

import java.util.List;

import com.job.pojo.JobEmpCmpyInfoMaster;
import com.job.pojo.JobUserInfoMaster;

public interface AdminServiceInterface {

	List<JobEmpCmpyInfoMaster> getAllCmpyList();

	List<JobUserInfoMaster> getAllUserList();

	List<JobEmpCmpyInfoMaster> getAllDeactiveCmpyList();

	List<JobUserInfoMaster> getAllDeactiveUserList();

	String deactivateUSer(int portalID,int activeFlag);

}
