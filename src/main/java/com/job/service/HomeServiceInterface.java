package com.job.service;

import com.job.pojo.JobEmpCmpyInfoMaster;
import com.job.pojo.JobUserInfoMaster;
import com.job.pojo.LoginTable;

public interface HomeServiceInterface {

	int createLogin(LoginTable loginTable);

	void updateUserProfile(JobUserInfoMaster infoMaster);

	void updateCompanyProfile(JobEmpCmpyInfoMaster empMaster);

	LoginTable checkLogin(String userName, String password);

	boolean validateRequest(String userName);

	String getFirstName(int portalID, String userType);

}
