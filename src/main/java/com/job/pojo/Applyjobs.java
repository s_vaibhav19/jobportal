package com.job.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ApplyJobs")
public class Applyjobs {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="AppliedId")
	private int applliedJobId;
	
	@Column(name="PortalID")
	private int portalId;
	
	@Column(name="JobID")
	private int jobId;
	
	@Column(name="FeedBackMsg")
	private String feedBackMsg;
	
	@Column(name="TimeStam")
	private String timeStamp;
	
	@Column(name="Status")
	private String status;

	public int getPortalId() {
		return portalId;
	}

	public void setPortalId(int portalId) {
		this.portalId = portalId;
	}

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public String getFeedBackMsg() {
		return feedBackMsg;
	}

	public void setFeedBackMsg(String feedBackMsg) {
		this.feedBackMsg = feedBackMsg;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
