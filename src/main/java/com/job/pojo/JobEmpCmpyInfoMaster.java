package com.job.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="job_Emp_CmpyInfo_master")
public class JobEmpCmpyInfoMaster {
	
	@Id
	@Column(name="PortalID")
	private int portalID;
	
	@Column(name="CompanyName")
	private String companyName;
	
	@Column(name="RegNo")
	private String regNo;
	
	@Column(name="ContactNo")
	private String contactNo;
	
	@Column(name="Email")
	private String email;

	@Column(name="WebSiteLink")
	private String webSiteLink;
	
	@Column(name="Descriptions")
	private String description;
	
	public int getPortalID() {
		return portalID;
	}
	public void setPortalID(int portalID) {
		this.portalID = portalID;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getRegNo() {
		return regNo;
	}
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getWebSiteLink() {
		return webSiteLink;
	}
	public void setWebSiteLink(String webSiteLink) {
		this.webSiteLink = webSiteLink;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
