package com.job.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SkillSet_Master")
public class SkillSetMaster {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	@Column(name="skillID")
	private int skillId;
	
	@Column(name="SkillSet")
	private String skillSet;
	
	public int getSkillId() {
		return skillId;
	}
	public void setSkillId(int skillId) {
		this.skillId = skillId;
	}
	public String getSkillSet() {
		return skillSet;
	}
	public void setSkillSet(String skillSet) {
		this.skillSet = skillSet;
	}
}
