package com.job.pojo;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cmp_job_Post_master")
public class CmpJobPostMaster {

	@Id
	@Column(name = "JobID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int jobId;

	@Column(name = "PortalID")
	private int portalID;

	@Column(name = "JobTitle")
	private String jobTitle;

	@Column(name = "JobDescription")
	private String jobDescription;

	@Column(name = "SkillSet")
	private String skillSet;

	@Column(name = "Location")
	private String location;

	@Column(name = "salary")
	private int salary;

	@Column(name = "NoOfVacancy")
	private int vacancy;

	@Column(name = "JobType")
	private String jobType;

	@Column(name = "TimeStamp")
	private String timeStamp;

	@Column(name = "ActiveFlag")
	private int activeFlag;

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public int getPortalID() {
		return portalID;
	}

	public void setPortalID(int portalID) {
		this.portalID = portalID;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	public String getSkillSet() {
		return skillSet;
	}

	public void setSkillSet(String skillSet) {
		this.skillSet = skillSet;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public int getVacancy() {
		return vacancy;
	}

	public void setVacancy(int vacancy) {
		this.vacancy = vacancy;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public int getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(int activeFlag) {
		this.activeFlag = activeFlag;
	}

}
