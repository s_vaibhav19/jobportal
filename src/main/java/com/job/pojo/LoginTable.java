package com.job.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="LoginTable")
public class LoginTable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="PortalID")
	private int portalID;
	
	@Column(name="UserName")
	private String userId;
	
	@Column(name="Password")
	private String password;
	
	@Column(name="UserType")
	private String userType;
	
	@Column(name="ActiveFlag")
	private int activeFlag;
	
	public int getPortalID() {
		return portalID;
	}
	public void setPortalID(int portalID) {
		this.portalID = portalID;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public int getActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(int activeFlag) {
		this.activeFlag = activeFlag;
	}

}
