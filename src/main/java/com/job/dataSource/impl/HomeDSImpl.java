package com.job.dataSource.impl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.job.dataSource.HomeDSInterface;
import com.job.pojo.JobEmpCmpyInfoMaster;
import com.job.pojo.JobUserInfoMaster;
import com.job.pojo.LoginTable;

@Repository("homeDsInterface")
public class HomeDSImpl implements HomeDSInterface {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public int createLogin(LoginTable loginTable) {
		sessionFactory.getCurrentSession().saveOrUpdate(loginTable);
		return loginTable.getPortalID();
	}

	@Override
	public void updateProfile(JobUserInfoMaster infoMaster) {
		sessionFactory.getCurrentSession().saveOrUpdate(infoMaster);
	}

	@Override
	public void updateCmpyProfile(JobEmpCmpyInfoMaster empMaster) {
		sessionFactory.getCurrentSession().saveOrUpdate(empMaster);
	}

	@Override
	public LoginTable checkLogin(String userName, String password) {

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LoginTable.class);
		criteria.add(Restrictions.eq("userId", userName));
		criteria.add(Restrictions.eq("password", password));
		criteria.add(Restrictions.eq("activeFlag", 1));

		return (LoginTable) criteria.uniqueResult();
	}

	@Override
	public boolean validateRequest(String userName) {
		Criteria checkUsesrId = sessionFactory.getCurrentSession().createCriteria(LoginTable.class);
		checkUsesrId.add(Restrictions.eq("userId", userName));
		
		
		if ( null == checkUsesrId.uniqueResult()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String getFirstName(int portalID, String userType) {
		if(userType.equals("EP1")){
			Criteria firstName = sessionFactory.getCurrentSession().createCriteria(JobEmpCmpyInfoMaster.class);
			firstName.add(Restrictions.eq("portalID",portalID));
			JobEmpCmpyInfoMaster infoMaster = (JobEmpCmpyInfoMaster) firstName.uniqueResult();
			return infoMaster.getCompanyName();
			
		}else if(userType.equals("NU2")){
			
			Criteria firstName = sessionFactory.getCurrentSession().createCriteria(JobUserInfoMaster.class);
			firstName.add(Restrictions.eq("portalID",portalID));
			JobUserInfoMaster infoMaster = (JobUserInfoMaster) firstName.uniqueResult();
			return infoMaster.getFirstName();
			
		}else if(userType.equals("AD0")){
			return "Admin";
		} 
		return null;
	}

}
