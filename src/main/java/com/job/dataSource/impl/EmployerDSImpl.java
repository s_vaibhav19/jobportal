package com.job.dataSource.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.job.dataSource.EmployerDSInterface;
import com.job.pojo.AppliedJobInfo;
import com.job.pojo.CmpJobPostMaster;
import com.job.pojo.JobEmpCmpyInfoMaster;
import com.job.pojo.JobUserInfoMaster;
import com.job.util.QuerysProvider;

@Repository("employerDSImpl")
public class EmployerDSImpl implements EmployerDSInterface {

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	QuerysProvider queryProvider;

	@Override
	public JobEmpCmpyInfoMaster getCmpyDetails(Object attribute) {
		Criteria cmpyInfo = sessionFactory.getCurrentSession().createCriteria(JobEmpCmpyInfoMaster.class);
		cmpyInfo.add(Restrictions.eq("portalID", Integer.parseInt(attribute.toString())));
		JobEmpCmpyInfoMaster infoMaster = (JobEmpCmpyInfoMaster) cmpyInfo.uniqueResult();
		return infoMaster;
	}

	@Override
	public void addJob(CmpJobPostMaster jobPostMaster) {
		sessionFactory.getCurrentSession().saveOrUpdate(jobPostMaster);
	}

	@Override
	public List<CmpJobPostMaster> getJobDetails(int portalID) {
		Criteria jobView = sessionFactory.getCurrentSession().createCriteria(CmpJobPostMaster.class);
		jobView.add(Restrictions.eq("portalID", portalID));
		List<CmpJobPostMaster> jobViewList = jobView.list();
		return jobViewList;
	}

	@Override
	public List<JobUserInfoMaster> getUserDetails(int portalID, int jobId) {
		List<JobUserInfoMaster> listOfUser = sessionFactory.getCurrentSession().createSQLQuery(queryProvider.getUSerDetailsQuery(portalID, jobId))
				.addScalar("portalID", Hibernate.INTEGER).addScalar("firstName", Hibernate.STRING).addScalar("lastName", Hibernate.STRING)
				.addScalar("email", Hibernate.STRING).addScalar("contactNo", Hibernate.STRING).addScalar("experience", Hibernate.INTEGER)
				.setResultTransformer(Transformers.aliasToBean(JobUserInfoMaster.class)).list();
		return listOfUser;
	}

	@Override
	public void updateFeedBAck(int jobId, int portalId, String status, String feedBackMsg) {
		sessionFactory.getCurrentSession().createSQLQuery("update applyjobs set FeedBackMsg=\"" + feedBackMsg + "\",Status=\"" + status
				+ "\" where PortalID=" + portalId + " and JobID=" + jobId + ";").executeUpdate();
	}

	@Override
	public String updateCmpyProfile(JobEmpCmpyInfoMaster masterInfo) {
		int count = sessionFactory.getCurrentSession()
				.createSQLQuery("update job_emp_cmpyinfo_master set CompanyName=\"" + masterInfo.getCompanyName() + "\",ContactNo=\""
						+ masterInfo.getContactNo() + "\",Descriptions=\"" + masterInfo.getDescription() + "\",Email=\"" + masterInfo.getEmail()
						+ "\",RegNo=\"" + masterInfo.getRegNo() + "\",WebSiteLink=\"" + masterInfo.getWebSiteLink().replaceAll("http://", "") + "\" where PortalID="
						+ masterInfo.getPortalID() + ";")
				.executeUpdate();
		if (count > 0)
			return "Profile Updated";
		else
			return "Error Updating Profile";
	}

	@Override
	public List<Integer> getResponded(int jobId) {
		List<Integer> portalIds = sessionFactory.getCurrentSession().createSQLQuery(queryProvider.getAppliedJobID(jobId)).list(); 
		return portalIds;
	}
}
