package com.job.dataSource.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.job.dataSource.AdminDSInterface;
import com.job.pojo.JobEmpCmpyInfoMaster;
import com.job.pojo.JobUserInfoMaster;
import com.job.util.QuerysProvider;

@Repository("adminDS")
public class AdminDSImpl implements AdminDSInterface {

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	QuerysProvider queryProvider;

	@Override
	public List<JobEmpCmpyInfoMaster> getCmpyList() {

		List<JobEmpCmpyInfoMaster> activeCmpList = sessionFactory.getCurrentSession().createSQLQuery(queryProvider.getCmpyList(1))
				.addScalar("portalID", Hibernate.INTEGER).addScalar("companyName", Hibernate.STRING).addScalar("regNo", Hibernate.STRING)
				.addScalar("contactNo", Hibernate.STRING).addScalar("webSiteLink", Hibernate.STRING).addScalar("description", Hibernate.STRING)
				.setResultTransformer(Transformers.aliasToBean(JobEmpCmpyInfoMaster.class)).list();

		return activeCmpList;
	}

	@Override
	public List<JobUserInfoMaster> getUserList() {

		List<JobUserInfoMaster> userList = sessionFactory.getCurrentSession().createSQLQuery(queryProvider.getUserList(1))
				.addScalar("portalID", Hibernate.INTEGER).addScalar("firstName", Hibernate.STRING).addScalar("lastName", Hibernate.STRING)
				.addScalar("contactNo", Hibernate.STRING).addScalar("email", Hibernate.STRING).addScalar("experience", Hibernate.INTEGER)
				.addScalar("salary", Hibernate.INTEGER).addScalar("location", Hibernate.STRING).setResultTransformer(Transformers.aliasToBean(JobUserInfoMaster.class)).list();
		return userList;
	}

	@Override
	public List<JobEmpCmpyInfoMaster> getCmpyDeactiveList() {
		List<JobEmpCmpyInfoMaster> activeCmpList = sessionFactory.getCurrentSession().createSQLQuery(queryProvider.getCmpyList(0))
				.addScalar("portalID", Hibernate.INTEGER).addScalar("companyName", Hibernate.STRING).addScalar("regNo", Hibernate.STRING)
				.addScalar("contactNo", Hibernate.STRING).addScalar("webSiteLink", Hibernate.STRING).addScalar("description", Hibernate.STRING)
				.setResultTransformer(Transformers.aliasToBean(JobEmpCmpyInfoMaster.class)).list();

		return activeCmpList;
	}

	@Override
	public List<JobUserInfoMaster> getDeactiveUserList() {
		List<JobUserInfoMaster> userList = sessionFactory.getCurrentSession().createSQLQuery(queryProvider.getUserList(0))
				.addScalar("portalID", Hibernate.INTEGER).addScalar("firstName", Hibernate.STRING).addScalar("lastName", Hibernate.STRING)
				.addScalar("contactNo", Hibernate.STRING).addScalar("email", Hibernate.STRING).addScalar("experience", Hibernate.INTEGER)
				.addScalar("salary", Hibernate.INTEGER).addScalar("location", Hibernate.STRING).setResultTransformer(Transformers.aliasToBean(JobUserInfoMaster.class)).list();
		return userList;
	}

	@Override
	public String deactivateUSer(int portalID,int activeFlag) {
		int updateCount = sessionFactory.getCurrentSession().createSQLQuery(queryProvider.getActDact(portalID,activeFlag)).executeUpdate();
		if(updateCount>0)
			return "Operation Performed";
		else
			return "Error While Performing Operation";
	}
	

}
