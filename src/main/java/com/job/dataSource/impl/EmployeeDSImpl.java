package com.job.dataSource.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.job.dataSource.EmployeeDSInterface;
import com.job.pojo.AppliedJobInfo;
import com.job.pojo.Applyjobs;
import com.job.pojo.CmpJobPostMaster;
import com.job.pojo.JobUserInfoMaster;
import com.job.util.QuerysProvider;

@Repository("employeeDS")
public class EmployeeDSImpl implements EmployeeDSInterface {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	QuerysProvider queryProvider;

	@Override
	public JobUserInfoMaster getUserDetails(Object attribute) {
		Criteria userinfo = sessionFactory.getCurrentSession().createCriteria(JobUserInfoMaster.class);
		userinfo.add(Restrictions.eq("portalID", Integer.parseInt(attribute.toString())));
		JobUserInfoMaster infoMaster = (JobUserInfoMaster) userinfo.uniqueResult();
		return infoMaster;
	}

	@Override
	public List<CmpJobPostMaster> getPostedJobs(int portalID) {
		
		List<CmpJobPostMaster> joblist = sessionFactory.getCurrentSession().createSQLQuery(queryProvider.getJobSearchlist(portalID)).addScalar("jobId",Hibernate.INTEGER).addScalar("portalID",Hibernate.INTEGER).addScalar("jobTitle",Hibernate.STRING).addScalar("jobDescription",Hibernate.STRING).addScalar("skillSet",Hibernate.STRING).addScalar("location",Hibernate.STRING).addScalar("salary",Hibernate.INTEGER).addScalar("vacancy",Hibernate.INTEGER).addScalar("jobType",Hibernate.STRING).addScalar("timeStamp",Hibernate.STRING).addScalar("activeFlag",Hibernate.INTEGER).setResultTransformer(Transformers.aliasToBean(CmpJobPostMaster.class)).list();
		return joblist;
	}

	@Override
	public String applyJob(int jobId, int portalId) {
		Applyjobs applyjobs = new Applyjobs();
		applyjobs.setJobId(jobId);
		applyjobs.setPortalId(portalId);
		applyjobs.setStatus("Hold");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(applyjobs);
			return "success";
		} catch (Exception e) {

		}
		return "error";
	}

	@Override
	public List<CmpJobPostMaster> getJobSearch(String location, String jobType, String skillSet) {
		Criteria jobsearch = sessionFactory.getCurrentSession().createCriteria(CmpJobPostMaster.class);
		if(!jobType.isEmpty())
			jobsearch.add(Restrictions.eq("jobType", jobType));
		
		if(!location.isEmpty())
			jobsearch.add(Restrictions.eq("location", location));
		
		if(!skillSet.isEmpty())
			jobsearch.add(Restrictions.eq("skillSet", skillSet));
		
		return jobsearch.list();
	}

	@Override
	public List<AppliedJobInfo> getAppliedLob(int portalId) {
		
		List<AppliedJobInfo> jobInfo = sessionFactory.getCurrentSession().createSQLQuery(queryProvider.getAppliedJob(portalId))
				.addScalar("jobId",Hibernate.INTEGER)
				.addScalar("jobTitle",Hibernate.STRING)
				.addScalar("location",Hibernate.STRING)
				.addScalar("status",Hibernate.STRING)
				.addScalar("message",Hibernate.STRING).setResultTransformer(Transformers.aliasToBean(AppliedJobInfo.class)).list();
		return jobInfo;
	}

	@Override
	public String updateUserJopb(JobUserInfoMaster masterInfo) {
		int countUpdated = sessionFactory.getCurrentSession().createSQLQuery(queryProvider.getUserUpdateQuery(masterInfo)).executeUpdate();
		if(countUpdated>0)
			return "Profile Updated Successfully";
		else
			return "Error Updating Profile";
	}

}
