package com.job.dataSource;

import java.util.List;

import com.job.pojo.JobEmpCmpyInfoMaster;
import com.job.pojo.JobUserInfoMaster;

public interface AdminDSInterface {

	List<JobEmpCmpyInfoMaster> getCmpyList();

	List<JobUserInfoMaster> getUserList();

	List<JobEmpCmpyInfoMaster> getCmpyDeactiveList();

	List<JobUserInfoMaster> getDeactiveUserList();

	String deactivateUSer(int portalID,int activeFlag);

}
