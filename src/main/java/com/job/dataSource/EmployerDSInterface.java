package com.job.dataSource;

import java.util.List;

import com.job.pojo.CmpJobPostMaster;
import com.job.pojo.JobEmpCmpyInfoMaster;
import com.job.pojo.JobUserInfoMaster;

public interface EmployerDSInterface {

	JobEmpCmpyInfoMaster getCmpyDetails(Object attribute);

	void addJob(CmpJobPostMaster jobPostMaster);

	List<CmpJobPostMaster> getJobDetails(int portalID);

	List<JobUserInfoMaster> getUserDetails(int portalID, int jobId);

	void updateFeedBAck(int jobId, int portalId, String status, String feedBackMsg);

	String updateCmpyProfile(JobEmpCmpyInfoMaster masterInfo);

	List<Integer> getResponded(int jobId);

}
