package com.job.dataSource;

import java.util.List;

import com.job.pojo.AppliedJobInfo;
import com.job.pojo.CmpJobPostMaster;
import com.job.pojo.JobUserInfoMaster;

public interface EmployeeDSInterface {

	JobUserInfoMaster getUserDetails(Object attribute);

	List<CmpJobPostMaster> getPostedJobs(int portalID);

	String applyJob(int jobId, int portalId);

	List<CmpJobPostMaster> getJobSearch(String location, String jobType, String skillSet);

	List<AppliedJobInfo> getAppliedLob(int portalId);

	String updateUserJopb(JobUserInfoMaster masterInfo);

}
