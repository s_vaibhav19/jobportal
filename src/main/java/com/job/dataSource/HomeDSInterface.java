package com.job.dataSource;

import com.job.pojo.JobEmpCmpyInfoMaster;
import com.job.pojo.JobUserInfoMaster;
import com.job.pojo.LoginTable;

public interface HomeDSInterface {

	int createLogin(LoginTable loginTable);

	void updateProfile(JobUserInfoMaster infoMaster);

	void updateCmpyProfile(JobEmpCmpyInfoMaster empMaster);

	LoginTable checkLogin(String userName, String password);

	boolean validateRequest(String userName);

	String getFirstName(int portalID, String userType);

}
