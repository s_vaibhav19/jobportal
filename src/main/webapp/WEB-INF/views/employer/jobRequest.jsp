<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<link rel="stylesheet"
	href="DataTables-1.10.10/media/css/jquery.dataTables.min.css">
<script src="DataTables-1.10.10/media/js/jquery.dataTables.min.js"></script>


<script>
	var constantPortalID = 0;
	$(document).ready(function() {
		$("#example").DataTable();
		loadInfoDiv();
		document.getElementById("popup").style.display = 'none';

	});

	function callDialog(portalID) {
		constantPortalID = portalID;
		if ($('#popup').is(':visible')) {
			document.getElementById("popup").style.display = 'none';
		} else {
			document.getElementById("popup").style.display = 'block';
		}

	}
	function submitFeedback() {
		var staus = $("#statusSelect").val();
		var feedMsg = $("#feedText").val();
		var jobId = $
		updateJobFeedBack(constantPortalID,staus,feedMsg);
	}
</script>
<style type="text/css">
.fillTh {
	background-color: buttonface;
}

.WhiteTh {
	background-color: white;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Employee Job Request Here</title>
</head>
<body>
	<div
		style="background-color: #33526E; color: white; text-align: center; padding: 1%;">
		<b> Posted Job Request </b>
	</div>
	<div>
		<div
			style="width: 30%; height: 90%; overflow: scroll; float: left; background-color: white;">
			&nbsp;&nbsp;<b>Job Title : </b> <select id="jobRequestSelect"
				onchange="loadInfoDiv()">
				<c:forEach var="jobInfo" items="${jobInfo}">
					<option value="${jobInfo.jobId}"><c:out
							value="${jobInfo.jobTitle}"></c:out>
					</option>
				</c:forEach>
			</select> <input type="button" value="Search" onclick="displayUserList()">
			<div id="inforamtionsDiv" style="background-color: whitesmoke;">
				<c:forEach var="jobInfo" items="${jobInfo}">
					<div id="infoDiv${jobInfo.jobId}" style="display: none;">
						<table width="99%;">
							<tr>
								<th class="fillTh">Job Title</th>
							</tr>
							<tr>
								<td><c:out value="${jobInfo.jobTitle}"></c:out></td>
							</tr>
							<tr>
								<th class="fillTh">Location</th>
							</tr>
							<tr>
								<td><c:out value="${jobInfo.location}"></c:out></td>
							</tr>
							<tr>
								<th class="fillTh">Skill Set</th>
							</tr>
							<tr>
								<td><c:out value="${jobInfo.skillSet}"></c:out></td>
							</tr>
							<tr>
								<th class="fillTh">Job Description</th>
							</tr>
							<tr>
								<td><c:out value="${jobInfo.jobDescription}"></c:out></td>
							</tr>
						</table>
					</div>
				</c:forEach>
			</div>
		</div>
		<div
			style="width: 70%; height: 90%; overflow: scroll; float: left; background-color: white;">

			<table id="example" class="display" cellspacing="0" width="100%">
				<thead>
					<th>Employee Name</th>
					<th>Contact Details</th>
					<th>Experience</th>
					<th>FeedBack</th>
				</thead>
				<tfoot>
					<th>Employee Name</th>
					<th>Contact Details</th>
					<th>Experience</th>
					<th>FeedBack</th>
				</tfoot>
			</table>
		</div>
		<div id="popup" title="Important information">
			<table width="100%">
				<tr>
					<th class="WhiteTh"><b>Status :</b></th>
				</tr>
				<tr align="center">
					<td><select id="statusSelect">
							<option>Hold</option>
							<option>Rejected</option>
							<option>Accepted</option>
					</select></td>
				</tr>
				<tr>
					<th class="WhiteTh"><b>Message :</b></th>
				</tr>
				<tr>
					<td><textarea rows="6" cols="39" maxlength="500"
							style="resize: none;" id="feedText"></textarea></td>
				</tr>
				<tr>
					<th class="WhiteTh"><input type="button" value="Update"
						onclick="submitFeedback()"></th>
				</tr>
			</table>

		</div>
	</div>



</body>
</html>