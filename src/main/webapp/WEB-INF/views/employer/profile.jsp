<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Profile</title>
</head>
<body>

	<div
		style="background-color: #33526E; color: white; text-align: center; padding: 1%;">
		<b> Company Profile </b>
	</div>
	<div style="height: 50%;">
		<div style="width: 45%; float: left; margin-top: 1%;">
			<table>
				<tr>
					<td><b> Company :</b></td>
					<td><input type="text" id="empCmpyName"
						value="${cmpyInfo.companyName}"></td>
				</tr>
				<tr>
					<td><b> Reg. No :</b></td>
					<td><input type="text" id="empRegNo" value="${cmpyInfo.regNo}">
					</td>
				</tr>
				<tr>
					<td><b> Email
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b></td>
					<td><input type="text" id="empEmail" value="${cmpyInfo.email}">	</td>
				</tr>
				<tr>
					<td><b> Contact No :</b></td>
					<td><input type="text" id="empContactNo"
						value="${cmpyInfo.contactNo}"></td>
				</tr>
				<tr>
					<td><b>WebSite : </b></td>
					<td><input type="text" id="empWebSite" value="${cmpyInfo.webSiteLink }">
					</td>
				</tr>
				<tr>
					<td><b> Description :</b></td>
					<td><textarea id="empDescription">${cmpyInfo.description } </textarea>
					</td>
				</tr>
				

			</table>
		</div>
		<div id="errormsgDiv" style="width: 45%; float: left; margin-top: 1%;"></div>
	</div>
	<div
		style="background-color: #33526E; color: white; text-align: center; padding: 1%;">
		<input type="button" value="UpDate" onclick="updateCmpyProile()">
	</div>

</body>
</html>