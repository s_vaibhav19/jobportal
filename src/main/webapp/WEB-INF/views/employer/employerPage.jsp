<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Welcome Company Home Page</title>

<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="CSS/masterCssFile.css">
<script src="DataTables-1.10.10/media/js/jquery-1.11.3.js"></script>

<style type="text/css">
.welcomeDiv {
	width: 98%;
	margin-left: 1%;
	height: auto;
	border-bottom: 1px solid gray;
}

.userPannel {
	width: 98%;
	margin-left: 1%;
	height: 100%;
}
#popup {
				width: 300px;
				height: 200px;
				position: absolute;
				color: white;
				background-color: slategrey;
				/* To align popup window at the center of screen*/
				top: 5%;
				left: 50%;
				margin-top: 1%;
				margin-left: -150px;
				border: 1px solid black;
			}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$(".menuItem").hover(function() {
			$(this).css("background-color", "#33526E");
			$(this).css("color", "white");
			$(this).css("cursor", "pointer");

		}, function() {
			$(this).css("background-color", "white");
			$(this).css("color", "black");
		});
	});
	
	function loadPage(str) {
		
		$("#loadingPanel").html("Loading... <i class=\"fa fa-refresh fa-spin\"></i>");
		 
		 $.ajax({
		        url : str+'.htm',
		        type: 'GET',

		        success: function(data){
		            $('#loadingPanel').html(data);
		        }
		    });
	}
	function updateCmpyProile() {
		
		var empCmpyName = $("#empCmpyName").val().trim();
		var empRegNo = $("#empRegNo").val().trim();
		var empEmail = $("#empEmail").val().trim();
		var empContactNo = $("#empContactNo").val().trim();
		var empWebSite = $("#empWebSite").val().trim();
		var empDescription = $("#jobDescriptions").val().trim();

		if(empCmpyName != "" && empRegNo  != "" && empEmail != "" && empContactNo != "" && empWebSite != "" && empDescription != ""){
			$.ajax({
		        url : 'updateCmpyProfile.htm',
		        type: 'POST',
				data: 'empCmpyName='+empCmpyName+'&empRegNo='+empRegNo+'&empEmail='+empEmail+'&empContactNo='+empContactNo+'&empWebSite='+empWebSite+'&empDescription='+empDescription,
		        success: function(data){
		            $('#errormsgDiv').html(data);
		        }
		    });
		}else{
			
		}
	}
	
	function postJob() {
		/* var jobType = $("#").val();
		var jobType = $("#").val(); */
		var jobTitle = $("#jobTitle").val().trim();
		var skillSet = $("#skillSet").val().trim();
		var salary = $("#salary").val().trim();
		var jobType = $("#jobType").val().trim();
		var vacancy = $("#vacancy").val().trim();
		var location = $("#location").val();
		var jobDescriptions = $("#jobDescriptions").val();
		
		if(jobTitle!= null && skillSet!= null && salary!= null && jobType != null){
			 
			 $.ajax({
			        url : 'jobPostAction.htm',
			        type: 'POST',
					data: 'jobTitle='+jobTitle+'&skillSet='+skillSet+'&salary='+salary+'&jobType='+jobType+'&vacancy='+vacancy+'&location='+location+'&jobDescriptions='+jobDescriptions,
			        success: function(data){
			            $('#loadingPanel').html(data);
			        }
			    });
		}else{
			
		}
	}
	
	function displayUserList() {
		var jobId = $("#jobRequestSelect").val();
		$.ajax({
	        url : 'getUserJobList.htm',
	        type: 'GET',
			data: 'jobId='+jobId,
	        success: function(data){
	        	$("#example").empty();
	        	$("#example").append(data);
	        }
	    });
		
	}
	
	function loadInfoDiv(){
		$("#inforamtionsDiv").children().hide();
		var divID = $("#jobRequestSelect").val();
		$("#infoDiv"+divID).show();
	}
	function updateJobFeedBack(portalID,staus,feedMsg) {
		
		var divID = $("#jobRequestSelect").val();
		$.ajax({
	        url : 'updateUSerFeedBack.htm',
	        type: 'POST',
			data: 'portalID='+portalID+"&staus="+staus+"&feedMsg="+feedMsg+"&divID="+divID,
	        success: function(data){
	        	
	        }
	    });
		callDialog(0);
	}
	
</script>
</head>
<body>
	<div class="mainDiv">

		<div class="header">
			<font
				style="margin-left: 1%; font-family: cursive; font-size: xx-large;">
				<b><i class="fa fa-users fa-1x"></i> Job-Portal </b>
			</font>
		</div>
		<div class="mainBody">
			<div class="welcomeDiv">
				<b><i class="fa fa-user"></i>&nbsp;&nbsp;Welcome, <%= (String)request.getSession().getAttribute("firstName") %></b> <span
					style="float: right;"> <b><a href="logout.htm">
							Sign Out </a></b>
				</span>
			</div>
			<div class="userPannel">
				<div
					style="float: left; width: 30%; height: 100%; border-right: 1px solid black;">
					<div class="verticalMenuDiv">
						<div class="menuItem" onclick="loadPage('cmpyProfile')"> Company Profile</div>
						<div class="menuItem" onclick="loadPage('jobPost')">Job Post</div>
						<div class="menuItem" onclick="loadPage('jobRequest')">Job Request</div>
						<div class="menuItem" onclick="loadPage('recentActivities')">Recent Activities</div>
					</div>
				</div>
				<div id="loadingPanel" style="float: left; width: 68%; height: 100%">
					
				</div>
			</div>
		</div>
		<div class="footer">Center Alligned Footer</div>
	</div>
</body>
</html>
