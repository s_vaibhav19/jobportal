<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Employee Job Post Here</title>
</head>
<body>
	<div
		style="background-color: #33526E; color: white; text-align: center; padding: 1%;">
		<b> Post Job </b>
	</div>
	<div>
		<table>
			<tr>
				<td>Job Title :  </td>
				<td><input type="text" id="jobTitle"> </td>
			</tr>
			<tr>
				<td>Skill Set  </td>
				<td><input type="text" id="skillSet"> </td>
			</tr>
			<tr>
				<td>Location </td>
				<td><select id="location"> <option>Mumbai</option><option>Pune</option><option>Nagpur</option><option>Delhi</option> </select> </td>
			</tr>
			<tr>
				<td>Job Description : </td>
				<td><textarea id="jobDescriptions"  rows="5" cols="30"> </textarea> </td>
			</tr>
			
			<tr>
				<td> Salary : </td>
				<td><input type="text" id="salary"> </td>
			</tr>
			<tr>
				<td> Job Type : </td>
				<td><select id="jobType" > <option>Part Time</option><option>Full Time</option><option>Internship</option> </select> </td>
			</tr>
			<tr>
				<td> Vacancy : </td>
				<td> <input type="text" id="vacancy"> </td>
			</tr>
		</table>
		
	</div>
	<div
		style="background-color: #33526E; color: white; text-align: center; padding: 1%;">
		<input type="button" value="Post Job" onclick="postJob()">
	</div>


</body>
</html>