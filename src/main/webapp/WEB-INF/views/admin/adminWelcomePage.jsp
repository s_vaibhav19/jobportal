<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Welcome Company Home Page</title>

<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="CSS/masterCssFile.css">
<script src="DataTables-1.10.10/media/js/jquery-1.11.3.js"></script>

<style type="text/css">
.welcomeDiv {
	width: 98%;
	margin-left: 1%;
	height: auto;
	border-bottom: 1px solid gray;
}

.userPannel {
	width: 98%;
	margin-left: 1%;
	height: 100%;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$(".menuItem").hover(function() {
			$(this).css("background-color", "#33526E");
			$(this).css("color", "white");
			$(this).css("cursor", "pointer");

		}, function() {
			$(this).css("background-color", "white");
			$(this).css("color", "black");
		});
	});
	
	function loadPage(str) {
		
		$("#loadingPanel").html("Loading... <i class=\"fa fa-refresh fa-spin\"></i>");
		 
		 $.ajax({
		        url : str+'.htm',
		        type: 'GET',

		        success: function(data){
		            $('#loadingPanel').html(data);
		        }
		    });
	}
	
	function deactivateUser(portalID,activityFlag,pageReload) {
		$.ajax({
	        url : 'deactivateUser.htm',
	        type: 'GET',
			data: 'portalID='+portalID+'&activityFlag='+activityFlag,
	        success: function(data){
	        	loadPage(pageReload);
	        }
	    });
	}
	
</script>
</head>
<body>
	<div class="mainDiv">

		<div class="header">
			<font
				style="margin-left: 1%; font-family: cursive; font-size: xx-large;">
				<b><i class="fa fa-users fa-1x"></i> Job-Portal </b>
			</font>
		</div>
		<div class="mainBody">
			<div class="welcomeDiv">
				<b><i class="fa fa-user"></i>&nbsp;&nbsp;Welcome, Admin</b> <span
					style="float: right;"> <b><a href="logout.htm">
							Sign Out </a></b>
				</span>
			</div>
			<div class="userPannel">
				<div
					style="float: left; width: 30%; height: 100%; border-right: 1px solid black;">
					<div class="verticalMenuDiv">
						<div class="menuItem" onclick="loadPage('adminCmpyList')"> Active Companies </div>
						<div class="menuItem" onclick="loadPage('adminUserList')"> Active Users </div>
						<div class="menuItem" onclick="loadPage('deactiveCmpList')">Deactive Companies </div>
						<div class="menuItem" onclick="loadPage('deactiveUserList')">Deactive Users </div>
					</div>
				</div>
				<div id="loadingPanel" style="float: left; width: 68%; height: 100%">
					
				</div>
			</div>
		</div>
		<div class="footer">Center Alligned Footer</div>
	</div>
</body>
</html>
