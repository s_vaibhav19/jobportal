<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="DataTables-1.10.10/media/css/jquery.dataTables.min.css">
<script src="DataTables-1.10.10/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#deactUser").DataTable();
});
</script>
</head>
</head>
<body>
<table id="deactUser">
	<thead>
		<th> Employee Name </th>
		<th> Contact No </th>
		<th> Email </th>
		<th> Location</th>
		<th> CoverLetter </th>
		<th>Operation</th>
	</thead>
	<tfoot>
		<th> Employee Name </th>
		<th> Contact No </th>
		<th> Email </th>
		<th> Location</th>
		<th> CoverLetter </th>
		<th>Operation</th>
	</tfoot>
	<tbody>
	<c:forEach var="userList" items="${userList}">
	<tr>
		<td><c:out value="${userList.firstName}"></c:out>&nbsp;<c:out value="${userList.lastName}"></c:out> </td>
		<td><c:out value="${userList.contactNo}"></c:out> </td>
		<td><c:out value="${userList.email}"></c:out> </td>
		<td><c:out value="${userList.location}"></c:out> </td>
		<td><c:out value="${userList.coverLetter}"></c:out> </td>
		<td><input type="button" value="Activate" onclick="deactivateUser(${userList.portalID},1,'deactiveUserList')"> </td>
	</tr>
	</c:forEach>
	</tbody>
</table>
</body>
</html>