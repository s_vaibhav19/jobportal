<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="DataTables-1.10.10/media/css/jquery.dataTables.min.css">
<script src="DataTables-1.10.10/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#deactCmpyList").DataTable();
});
</script>
</head>
<body>
	<table id="deactCmpyList">
		<thead>
			<th>Company Name</th>
			<th>Contact No</th>
			<th>Email </th>
			<th>WebSite</th>
			<th>Information</th>
			<th>Operation</th>
		</thead>
		<tfoot>
			<th>Company Name</th>
			<th>Contact No</th>
			<th>Email </th>
			<th>WebSite</th>
			<th>Information</th>
			<th>Operation</th>
		</tfoot>
		<tbody>
		<c:forEach var="cmpyList" items="${cmpyList}">
		<tr>
			<td> <c:out value="${cmpyList.companyName}"></c:out> </td>
			<td> <c:out value="${cmpyList.contactNo}"></c:out> </td>
			<td> <c:out value="${cmpyList.email}"></c:out> </td>
			<td> <c:out value="${cmpyList.webSiteLink}"></c:out> </td>
			<td> <c:out value="${cmpyList.description}"></c:out> </td>
			<td><input type="button" value="Activate" onclick="deactivateUser(${cmpyList.portalID},1,'deactiveCmpList')"> </td>
		</tr>
		</c:forEach>
		</tbody>
	</table>

</body>
</html>