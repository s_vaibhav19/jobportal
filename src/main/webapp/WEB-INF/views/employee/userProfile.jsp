<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Profile</title>
</head>
<body>

	<div style="background-color: #33526E; color: white;text-align: center;padding: 1%;">
	<b>	User Profile </b>
	</div>
	<div style="height: 50%;">
	<div style="width: 45%;float: left;margin-top: 1%;">
		<table>
			<tr>
				<td> First Name : </td>
				<td> <input type="text" id="userFirstName" value="${userTable.firstName}"> </td>
			</tr>
			<tr>
				<td> Last Name :  </td>
				<td> <input type="text" id="userLastName" value="${userTable.lastName}"> </td>
			</tr>
			<tr>
				<td> Email : </td>
				<td> <input type="text" id="userEmail" value="${userTable.email}"> </td>
			</tr>
			<tr>
				<td> Mobile No </td>
				<td> <input type="text" id="userNo" value="${userTable.contactNo}"> </td>
			</tr>
			<tr>
				<td>  </td>
				<td>  </td>
			</tr>
			<tr>
				<td>  </td>
				<td>  </td>
			</tr>
		</table>
	</div>
	<div style="width: 45%;float: left;margin-top: 1%;">
		<table>
			<tr>
				<td> Location : </td>
				<td> <input type="text" id="userLocation" value="${userTable.location}"> </td>
			</tr>
			<tr>
				<td> Experiance : </td>
				<td> <input type="text" id="userExp" value="${userTable.experience}"> </td>
			</tr>
			<tr>
				<td> Salary :  </td>
				<td> <input type="text" id="userSalary" value="${userTable.salary}"> </td>
			</tr>
			
			<tr>
				<td> Cover Letter : </td>
				<td> <textarea id="coverLetter" maxlength="499" >${userTable.coverLetter} </textarea></td>
			</tr>
			<tr>
				<td > </td>
			</tr>
		</table>
		
	</div>
	<div id="errorDiv" style="background-color: red;color: white; text-align: center; width: 100%;"> </div>
	</div>
	<div style="background-color: #33526E; color: white;text-align: center;padding: 1%;"> <input type="button" value="UpDate" onclick="updateUserProfile()"> </div>
	
	</body>
</html>