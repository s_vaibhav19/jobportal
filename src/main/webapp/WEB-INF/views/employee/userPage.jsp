<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<html>
<head>
<title>Welcome User Home Page</title>
<link rel="stylesheet"
	href="DataTables-1.10.10/media/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="CSS/masterCssFile.css">
<script src="resources/jquery-1.11.3.js"></script>
<script src="DataTables-1.10.10/media/js/jquery.dataTables.min.js"></script>
<style type="text/css">
.welcomeDiv {
	width: 98%;
	margin-left: 1%;
	height: auto;
	border-bottom: 1px solid gray;
}

.userPannel {
	width: 98%;
	margin-left: 1%;
	height: 100%;
}

tr:nth-child(even) {
	background-color: buttonface;
}

.evenFace {
	background-color: white;
	color: black;
	text-align: center;
	float: left;
	width: 29%;
	border: 1px solid black;
}

.oddFace {
	background-color: #3294D6;
	color: white;
	text-align: center;
	float: left;
	width: 20%;
	border: 1px solid black;
}
.jobDesc{
	background-color: white;
	color: black;
	text-align: left;
	font-family: cursive;
	width: 100%;
}
.btn {
  background: #3498db;
  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
  background-image: -o-linear-gradient(top, #3498db, #2980b9);
  background-image: linear-gradient(to bottom, #3498db, #2980b9);
  -webkit-border-radius: 28;
  -moz-border-radius: 28;
  border-radius: 28px;
  font-family: Georgia;
  color: #ffffff;
  font-size: 15px;
  padding: 10px 20px 10px 20px;
  text-decoration: none;
}

.btn:hover {
  background: #3cb0fd;
  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
  text-decoration: none;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$(".menuItem").hover(function() {
			$(this).css("background-color", "#33526E");
			$(this).css("color", "white");
			$(this).css("cursor", "pointer");

		}, function() {
			$(this).css("background-color", "white");
			$(this).css("color", "black");
		});

		$("#jobTable").DataTable();
	});

	function loadPage(str) {

		$("#loadingPanel").html("Loading... <i class=\"fa fa-refresh fa-spin\"></i>");

		$.ajax({
			url : str + '.htm',
			type : 'GET',
			success : function(data) {
				$('#loadingPanel').html(data);
			}
		});

	}
	function updateUserProfile() {

		var userFirstName = $("#userFirstName").val();
		var userLastName = $("#userLastName").val();
		var userEmail = $("#userEmail").val();
		var userNo = $("#userNo").val();
		var userExp = $("#userExp").val();
		var userSalary = $("#userSalary").val();
		var userLocation =$("#userLocation").val();
		var coverLetter = $("#coverLetter").val();
		
		if (userFirstName != '' && userLastName != '' && userEmail != ''
				&& userNo != '' && userExp != ''
				&& userSalary != '') {
			
			$.ajax({
				url : 'updateUserProfile.htm',
				type : 'POST',
				data : 'userFirstName='+userFirstName+"&userLastName="+userLastName+"&userEmail="+userEmail+"&userNo="+userNo+"&userExp="+userExp+"&userSalary="+userSalary+"&userLocation="+userLocation+"&coverLetter="+coverLetter,
				success : function(data) {
					$('#loadingPanel').html(data);
				}
			});
				
		} else {
			$("#errorDiv").html("Error While Updating Profile");
		}
	}

	function searchJob() {
		var locations = $("#joblocation").val().trim();
		var jobType = $("#jobType").val().trim();
		var skillSet = $("#skillSet").val().trim();
		
		$.ajax({
			url : 'searhJobCreteria.htm',
			type : 'POST',
			data : 'locations='+locations+"&jobType="+jobType+"&skillSet="+skillSet,
			success : function(data) {
				$('#loadingPanel').html(data);
			}
		});

	}
	function applyJob(jobID) {
		$.ajax({
			url : 'applyThisJob.htm',
			type : 'POST',
			data : 'jobID='+jobID,
			success : function(data) {
				if(data == "success"){
					$('#jobId'+jobID).html("<font color=\"green\"><b> Job Applied Sucessfully </b></font>");
				}else{
				$('#jobId'+jobID).html("<input class=\"btn\" type=\"button\" value=\"Apply This Job\" onclick=\"applyJob("+jobID+")\"><font color=\"red\"> Could not apply for this job please try again</font>");
				}
			}
		});
	}
</script>
</head>
<body>
	<div class="mainDiv">

		<div class="header">
			<font
				style="margin-left: 1%; font-family: cursive; font-size: xx-large;">
				<b><i class="fa fa-users fa-1x"></i> Job-Portal </b>
			</font>
		</div>
		<div class="mainBody">
			<div class="welcomeDiv">
				<b><i class="fa fa-user"></i>&nbsp;&nbsp;Welcome, <%=(String) request.getSession().getAttribute("firstName")%>
				</b> <span style="float: right;"> <b><a href="logout.htm">
							Sign Out </a></b>
				</span>
			</div>
			<div class="userPannel">
				<div
					style="float: left; width: 30%; height: 100%; border-right: 1px solid black;">
					<div class="verticalMenuDiv">
						<div class="menuItem" onclick="loadPage('myProfile')">My
							Profile</div>
						<div class="menuItem" onclick="loadPage('jobSearch')">Job
							Search</div>
						<div class="menuItem" onclick="loadPage('appliedJobs')">Applied Jobs</div>
					</div>
				</div>
				<div id="loadingPanel" style="float: left; width: 68%; height: 100%">

				</div>
			</div>
		</div>

		<div class="footer">Center Alligned Footer</div>
	</div>
</body>
</html>
