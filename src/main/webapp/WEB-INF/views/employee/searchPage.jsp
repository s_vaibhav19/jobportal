<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div
	style="text-align: center; border: 1 medium blue; padding: 1%; background-color: #33526E; color: white;">
	<b>Search Job :&nbsp;&nbsp; </b> <b>Location</b> <select
		id="joblocation">
		<option>Mumbai</option>
		<option>Delhi</option>
		<option>Nagpur</option>
		<option>Banglore</option>
		<option>Pune</option>
	</select> <b>Job Type :</b> <select id="jobType">
		<option>Part Time</option>
		<option>Full Time</option>
		<option>Internship</option> 
	</select> 
	<b>Skills Sets</b> <input type="text" id="skillSet"> <input
		type="button" value="Search" onclick="searchJob()">
</div>
<div style="width: 100%;overflow: scroll;height: 90%;">
	<table id="jobTable" style="width: 100%;">
		
		<tbody>
		<c:forEach var="jobList" items="${jobList}">
		<tr>
			<td>
					<div class="jobList" style="width: 100%;height: auto;border: 1px solid blue;">
						<div class="oddFace"><b>Job Title :&nbsp;</b></div>
						<div class="evenFace"><b> <c:out value="${jobList.jobTitle}"></c:out></b></div>
						<div class="oddFace"><b>Job Location :&nbsp;</b></div>
						<div class="evenFace"><b><c:out value="${jobList.location}"></c:out></b></div>
						<div class="oddFace"><b>Vacancy :&nbsp;</b></div>
						<div class="evenFace"><b> <c:out value="${jobList.vacancy}"></c:out></b></div>
						<div class="oddFace"><b>Skill Sets :&nbsp;</b></div>
						<div class="evenFace"><b><c:out value="${jobList.skillSet}"></c:out></b></div>
						<div class="oddFace"><b>Salary :&nbsp;</b></div>
						<div class="evenFace"><b> <c:out value="${jobList.salary}"></c:out></b></div>
						<div class="oddFace"><b>Job type :&nbsp;</b></div>
						<div class="evenFace"><b><c:out value="${jobList.jobType}"></c:out></b></div>
						<div class="jobDesc">
						 <b><u> Job Descriptions :</u> </b><br>
						<c:out value="${jobList.jobDescription}"></c:out>
						<hr style="color: black;">
						<div id="jobId${jobList.jobId}">
						<input class="btn" type="button" value="Apply This Job" onclick="applyJob(${jobList.jobId})">
						
						</div>
						</div>
					</div>
				</td>
				</tr>
				<tr style="background-color: white;border: 1px solid gray;">
				<td> </td>
				</tr>
		</c:forEach>
		</tbody>
	</table>
</div>