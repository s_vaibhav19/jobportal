<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Arbeit-Platz</title>

<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="CSS/masterCssFile.css">
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>

<script type="text/javascript">
	function checkLogin() {

		var userKey = $("#userKey").val();
		var passKey = $("#passKey").val();
		if (userKey != '' && passKey != '') {

			var dataString = 'userKey=' + userKey + "&passkey=" + passKey;
			commonLoginProcess("errorLogin", "loginAction.htm", dataString);

		} else {
			$("#errorLogin").html("UserName/Password is incorrect");
		}

	}
	function userReg() {
		var firstName = $("#firstName").val();
		var lastName = $("#lastName").val();
		var userEmail = $("#userEmail").val();
		var userNo = $("#userNo").val();
		var userPassword = $("#userPassword").val();

		if (firstName != '' && lastName != '' && userEmail != ''
				&& userNo != '' && userPassword != '') {

			var dataStr = 'firstName=' + firstName + "&lastName=" + lastName
					+ "&userEmail=" + userEmail + "&userNo=" + userNo
					+ "&userPassword=" + userPassword;
			commonLoginProcess("errorReg1", "registerUser.htm", dataStr);

		} else {
			$("#errorReg1").html("Some Informations is Missing");
		}
	}

	function cmpyReg() {
		var cmpyName = $("#cmpyName").val();
		var regNo = $("#regNo").val();
		var cmpyEmail = $("#cmpyEmail").val();
		var cmpyNo = $("#cmpyNo").val();
		var cmpyPasskey = $("#cmpyPasskey").val();

		if (cmpyName != '' && regNo != '' && cmpyEmail != '' && cmpyNo != ''
				&& cmpyPasskey != '') {

			var dataStr = 'cmpyName=' + cmpyName + "&regNo=" + regNo+"&cmpyEmail="+cmpyEmail+"&cmpyNo="+cmpyNo+"&cmpyPasskey="+cmpyPasskey;
			commonLoginProcess("errorReg2", "registerCmpy.htm", dataStr);
		} else {
			$("#errorReg2").html("Some Informations is Missing");
		}
	}

	function commonLoginProcess(divAddress, actionURL, dataString) {
		$("#" + divAddress)
				.html(
						" <i class=\"fa fa-refresh fa-spin\" style=\"color: green;\"> </i>");

		$.ajax({
			url : actionURL,
			type : 'POST',
			data : dataString,
			success : function(data) {
				if (data == 'divertSession')
					window.location.assign("forwardRequest.htm");
				else
					$("#" + divAddress).html("Invalid Credentials");
			}
		});
	}

	function changeRegisterTable() {
		if ($("#selectTypeReg").val() == 1) {
			$("#employerTable").hide();
			$("#employeeTable").show();
		} else {
			$("#employeeTable").hide();
			$("#employerTable").show();
		}
	}
	$('document').ready(function() {
		$("#employerTable").hide();
		$("#employeeTable").show();
	});
</script>
<style type="text/css">
#detailsDiv {
	float: left;
	width: 55%;
	height: 100%;
	margin-top: 5%;
	text-align: center;
}

#loginDiv {
	padding: 2%;
	float: right;
	width: auto;
	height: auto;
	margin-top: 1%;
	margin-right: 2%;
	border: 1px solid white;
}
</style>
</head>
<body>
	<div class="mainDiv">

		<div class="header">
			<font
				style="margin-left: 1%; font-family: cursive; font-size: xx-large;">
				<b><i class="fa fa-users fa-1x"></i> Job-Portal </b>
			</font>
		</div>
		<div class="mainBody">
			<div id="detailsDiv"></div>
			<div id="loginDiv">
				<table style="color: #33526E; border: 1px solid #33526E;">
					<tr style="background-color: #33526E;">
						<td colspan="2" align="left" style="color: white;"><b>
								&nbsp;&nbsp;&nbsp; <i class="fa fa-lock"></i> &nbsp;Log In
						</b></td>
					</tr>
					<tr>
						<td><b>User Name :</b></td>
						<td><input type="text" id="userKey"></td>
					</tr>
					<tr>
						<td><b> Password &nbsp;&nbsp;: </b></td>
						<td><input type="password" id="passKey"></td>
					</tr>
					<tr>
						<td colspan="2" align="center" id="errorLogin" style="color: red;">&nbsp;
						</td>
					</tr>
					<tr align="right">
						<td colspan="2"><input type="button" value="login"
							onclick="checkLogin()"></td>
					</tr>

				</table>
				<table
					style="color: #33526E; border-left: 1px solid #33526E; border-top: 1px solid #33526E; border-right: 1px solid #33526E; margin-top: 1%;">
					<tr style="background-color: #33526E;">
						<td colspan="2" align="left" style="color: white;">
							&nbsp;&nbsp; <B><i class="fa fa-sign-in"></i> &nbsp;Register
								Here</B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
					</tr>
					<tr>
						<td><B>As&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								:</B></td>
						<td><select id="selectTypeReg"
							onchange="changeRegisterTable()">
								<option value="1">Employee</option>
								<option value="2">Employer</option>
						</select></td>
					</tr>
				</table>
				<table id="employeeTable"
					style="color: #33526E; border-left: 1px solid #33526E; border-bottom: 1px solid #33526E; border-right: 1px solid #33526E;">

					<tr>
						<td><b> First Name :</b></td>
						<td><input type="text" id="firstName"></td>
					</tr>
					<tr>
						<td><b> Last Name :</b></td>
						<td><input type="text" id="lastName"></td>
					</tr>
					<tr>
						<td><b> Email
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b></td>
						<td><input type="text" id="userEmail"></td>
					</tr>
					<tr>
						<td><b> Mobile No :</b></td>
						<td><input type="text" id="userNo"></td>
					</tr>
					<tr>
						<td><b> Password &nbsp;: </b></td>
						<td><input type="password" id="userPassword"></td>
					</tr>
					<tr>
						<td colspan="2" align="center" id="errorReg1" style="color: red;">&nbsp;
						</td>
					</tr>
					<tr>
						<td colspan="2" align="right"><input type="button"
							value="Register" onclick="userReg()"></td>
					</tr>
				</table>
				<table id="employerTable"
					style="color: #33526E; border-left: 1px solid #33526E; border-bottom: 1px solid #33526E; border-right: 1px solid #33526E;">

					<tr>
						<td><b> Company :</b></td>
						<td><input type="text" id="cmpyName"></td>
					</tr>
					<tr>
						<td><b> Reg. No :</b></td>
						<td><input type="text" id="regNo"></td>
					</tr>
					<tr>
						<td><b> Email
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b></td>
						<td><input type="text" id="cmpyEmail"></td>
					</tr>
					<tr>
						<td><b> Contact No :</b></td>
						<td><input type="text" id="cmpyNo"></td>
					</tr>
					<tr>
						<td><b> Password &nbsp;: </b></td>
						<td><input type="password" id="cmpyPasskey"></td>
					</tr>
					<tr>
						<td colspan="2" align="center" id="errorReg2" style="color: red;">&nbsp;
						</td>
					</tr>
					<tr>
						<td colspan="2" align="right"><input type="button"
							value="Register" onclick="cmpyReg()"></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="footer">Designed And Developed By Vaibhav Sakharkar,
			Mayank Singh, Vatsan .</div>
	</div>
</body>
</html>
